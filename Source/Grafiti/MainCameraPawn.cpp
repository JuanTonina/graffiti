// Fill out your copyright notice in the Description page of Project Settings.


#include "MainCameraPawn.h"

#include <string>


#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"

// Sets default values
AMainCameraPawn::AMainCameraPawn()
{
    // Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    //Create our components
    RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
    MainCameraSpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraSpringArm"));
    MainCameraSpringArm->SetupAttachment(RootComponent);
    MainCameraSpringArm->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, 50.0f), FRotator(-60.0f, 0.0f, 0.0f));
    MainCameraSpringArm->TargetArmLength = 400.f;
    MainCameraSpringArm->bEnableCameraLag = true;
    MainCameraSpringArm->CameraLagSpeed = 3.0f;

    MainCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("GameCamera"));
    MainCamera->SetupAttachment(MainCameraSpringArm, USpringArmComponent::SocketName);

    //Take control of the default Player
    AutoPossessPlayer = EAutoReceiveInput::Player0;
}

// Called when the game starts or when spawned
void AMainCameraPawn::BeginPlay()
{
    Super::BeginPlay();
}

// Called every frame
void AMainCameraPawn::Tick(const float DeltaTime)
{
    Super::Tick(DeltaTime);

    //Zoom in if ZoomIn button is down, zoom back out if it's not
    {
        if (bZoomingIn)
        {
            ZoomFactor += DeltaTime / 0.5f; //Zoom in over half a second
        }
        else
        {
            ZoomFactor -= DeltaTime / 0.25f; //Zoom out over a quarter of a second
        }
        ZoomFactor = FMath::Clamp<float>(ZoomFactor, 0.0f, 1.0f);

        //Blend our camera's FOV and our SpringArm's length based on ZoomFactor
        MainCamera->FieldOfView = FMath::Lerp<float>(90.0f, 60.0f, ZoomFactor);
        MainCameraSpringArm->TargetArmLength = FMath::Lerp<float>(MaxDistance, MinDistance, ZoomFactor);
    }

    //Handle movement based on our "MoveX" and "MoveY" axes
    {
        if (!MovementInput.IsZero())
        {
            const float ActualSpeed = bFastMove ? MovementSpeed * SpeedModifier : MovementSpeed;
            //Scale our movement input axis values by 100 units per second
            MovementInput = MovementInput.GetSafeNormal() * ActualSpeed;
            FVector NewLocation = GetActorLocation();
            NewLocation += GetActorForwardVector() * MovementInput.X * DeltaTime;
            NewLocation += GetActorRightVector() * MovementInput.Y * DeltaTime;
            SetActorLocation(NewLocation);
        }
    }

    //Rotate our actor's yaw, which will turn our camera because we're attached to it
    {
        FRotator NewRotation = GetActorRotation();
        NewRotation.Yaw += CameraInput.X;
        SetActorRotation(NewRotation);
    }
}

// Called to bind functionality to input
void AMainCameraPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);

    //Hook up events for "ZoomIn"
    InputComponent->BindAction("ZoomIn", IE_Pressed, this, &AMainCameraPawn::ZoomIn);
    InputComponent->BindAction("ZoomOut", IE_Pressed, this, &AMainCameraPawn::ZoomOut);

    //Hook up events for "Run"
    InputComponent->BindAction("Run", IE_Pressed, this, &AMainCameraPawn::Run);
    InputComponent->BindAction("Run", IE_Released, this, &AMainCameraPawn::SlowMove);

    //Hook up every-frame handling for our three axes
    InputComponent->BindAxis("MoveForward", this, &AMainCameraPawn::MoveForward);
    InputComponent->BindAxis("MoveRight", this, &AMainCameraPawn::MoveRight);
    InputComponent->BindAxis("YawCamera", this, &AMainCameraPawn::YawCamera);
}

//Input functions
void AMainCameraPawn::MoveForward(const float AxisValue)
{
    MovementInput.X = FMath::Clamp<float>(AxisValue, -1.0f, 1.0f);
}

void AMainCameraPawn::MoveRight(const float AxisValue)
{
    MovementInput.Y = FMath::Clamp<float>(AxisValue, -1.0f, 1.0f);
}

void AMainCameraPawn::YawCamera(const float AxisValue)
{
    CameraInput.X = FMath::Clamp<float>(AxisValue, -1.0f, 1.0f);
}

//Input functions
void AMainCameraPawn::Run()
{
    bFastMove = true;
}

void AMainCameraPawn::SlowMove()
{
    bFastMove = false;
}

void AMainCameraPawn::ZoomIn()
{
    bZoomingIn = true;
}

void AMainCameraPawn::ZoomOut()
{
    bZoomingIn = false;
}
