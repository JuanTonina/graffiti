// Fill out your copyright notice in the Description page of Project Settings.


#include "CellActor.h"

// Sets default values for this component's properties
ACellActor::ACellActor()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
    RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
    PlaneComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PlaneComponent"));
    PlaneComponent->SetupAttachment(RootComponent);
    PlaneComponent->SetRelativeLocationAndRotation(FVector(0.f, 0.f, 0.f), FRotator(0.f, 0.f, 0.f));
    static ConstructorHelpers::FObjectFinder <UStaticMesh>CellMesh(TEXT("StaticMesh'/Game/Geometry/Meshes/plane.plane'"));
    static ConstructorHelpers::FObjectFinder <UMaterial>CellMaterial(TEXT("Material'/Game/Geometry/Meshes/Material_001.Material_001'"));

    PlaneComponent->SetStaticMesh(CellMesh.Object);
    PlaneComponent->SetMaterial(0, CellMaterial.Object);
}


// Called when the game starts
void ACellActor::BeginPlay()
{
    Super::BeginPlay();

    // ...
}


// Called every frame
void ACellActor::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}
