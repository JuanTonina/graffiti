// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CellActor.generated.h"


class UStaticMeshComponent;
UCLASS()
class GRAFITI_API ACellActor final : public AActor
{
    GENERATED_BODY()

public:
    // Sets default values for this component's properties
    ACellActor();
   
protected:
    // Called when the game starts
    virtual void BeginPlay() override;

    // Editable properties
    UPROPERTY(EditAnywhere)
    int Height = 0;
    UPROPERTY(EditAnywhere)
    bool Traversable = true;
    UPROPERTY(EditAnywhere)
    bool Occupable = true;
    UPROPERTY(EditAnywhere)
    AActor* Occupant;
    UPROPERTY(EditAnywhere)
    FColor Color;
    UPROPERTY(EditAnywhere)
    int MovementCost = 1;
    UPROPERTY(EditAnywhere)
    UStaticMeshComponent* PlaneComponent;


public:
    // Called every frame
    virtual void Tick(float DeltaTime) override;
};
