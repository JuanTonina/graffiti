// Fill out your copyright notice in the Description page of Project Settings.


#include "GridActor.h"

#include <string>

// Sets default values
AGridActor::AGridActor()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = false;

    RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
}

// Called when the game starts or when spawned
void AGridActor::BeginPlay()
{
    Super::BeginPlay();
}

// Called every frame
void AGridActor::Tick(const float DeltaTime)
{
    Super::Tick(DeltaTime);
}

void AGridActor::DestroyCells()
{
    for (auto CellActors : CellMatrix)
    {
        for (auto CellActor : CellActors)
        {
            CellActor->Destroy();
        }
    }
    CellMatrix.Empty();
}

void AGridActor::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
    Super::PostEditChangeProperty(PropertyChangedEvent);
    const FString Name = PropertyChangedEvent.Property->GetName();
    if (Name == FString("SizeX") || Name == FString("SizeY"))
    {
        DestroyCells();
        GenerateCells();
    }
}

void AGridActor::GenerateCells()
{
    const FVector NewCellLocation = this->GetActorLocation();
    FVector* NewCellLocationPointer = new FVector(NewCellLocation.X, NewCellLocation.Y, NewCellLocation.Z);
    for (int i = 0; i < SizeX; ++i)
    {
        TArray<ACellActor*> Row;
        for (int j = 0; j < SizeY; ++j)
        {
            UWorld* World = AActor::GetWorld();
            if (World != nullptr)
            {
                ACellActor* Cell = static_cast<ACellActor*>(World->SpawnActor(ACellActor::StaticClass(),
                                                                              NewCellLocationPointer));
                Row.Add(Cell);
                Cell->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
                NewCellLocationPointer = new FVector(NewCellLocationPointer->X, NewCellLocationPointer->Y + 200.f,
                                                     NewCellLocationPointer->Z);
            }
        }
        NewCellLocationPointer = new FVector(NewCellLocationPointer->X + 200.f, NewCellLocation.Y,
                                             NewCellLocationPointer->Z);

        CellMatrix.Add(Row);
    }
}

void AGridActor::PostInitProperties()
{
    Super::PostInitProperties();
    if (!IsTemplate(RF_Transient))
    {
        GenerateCells();
    }
}
