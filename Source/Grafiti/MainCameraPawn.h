// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "MainCameraPawn.generated.h"

class UCameraComponent;
class USpringArmComponent;
UCLASS()
class GRAFITI_API AMainCameraPawn : public APawn
{
    GENERATED_BODY()

public:
    // Sets default values for this pawn's properties
    AMainCameraPawn();

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

public:
    // Called every frame
    virtual void Tick(float DeltaTime) override;

    // Called to bind functionality to input
    virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

protected:
    UPROPERTY(EditAnywhere)
    USpringArmComponent* MainCameraSpringArm;
    UCameraComponent* MainCamera;

    //Input variables
    FVector2D MovementInput;
    FVector2D CameraInput;
    float ZoomFactor;
    bool bZoomingIn;
    bool bFastMove;

    UPROPERTY(EditAnywhere)
    float MinDistance = 300.f;
    UPROPERTY(EditAnywhere)
    float MaxDistance = 700.f;
    UPROPERTY(EditAnywhere)
    float MovementSpeed = 500.0f;
    UPROPERTY(EditAnywhere)
    float SpeedModifier = 2.f;
    bool bYawCamera = false;

    //Input functions
    void MoveForward(float AxisValue);
    void MoveRight(float AxisValue);
    void YawCamera(float AxisValue);
    void Run();
    void SlowMove();
    void ZoomIn();
    void ZoomOut();
};
