// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CellActor.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GridActor.generated.h"

UCLASS()
class GRAFITI_API AGridActor final : public AActor
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	AGridActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void PostInitProperties() override;

	TArray<TArray<ACellActor*>> CellMatrix;
	UPROPERTY(EditAnywhere)
    int SizeX = 10;
	UPROPERTY(EditAnywhere)
    int SizeY = 10;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void DestroyCells();
	virtual void PostEditChangeProperty(FPropertyChangedEvent &PropertyChangedEvent) override;
	void GenerateCells();
};
